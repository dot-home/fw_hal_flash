//********************************************************************************
/*!
\author     
\date       

\file       HAL_Flash
\brief      Interface of module for storing of user and system settings into device flash.
            This part is called "User Supervisory Flash (SFLASH)".

\details    This module handles storing of settings in the nonvolatile memory. 
            For this case two of four user rows taken. 2 rows taken to be immutable 
            against power failure during writing. The incomming data written always
            into row with oldest data. Data in both rows have same layout:
*/
            
#include "TargetConfig.h"
#ifdef CYPRESS_PSOC4100S_PLUS
            
#include <string.h>
#include "HAL_Flash.h"
#include "CyFlash.h"

/****************************************** Defines ******************************************************/
#define WRITTEN_MAGIC_NUMBER (0xCAFEBABE) /*< Magic numbers for written row */

//! Used for storing rows
#define FLASH_ROW_INSTANCE1           (0)
#define FLASH_ROW_INSTANCE2           (1)

//! Addresses of used rows in flash
#define ADDR_FLASH_ROW_INSTANCE1      (CY_SFLASH_USERBASE + FLASH_ROW_INSTANCE1 * (CY_SFLASH_SIZEOF_USERROW))
#define ADDR_FLASH_ROW_INSTANCE2      (CY_SFLASH_USERBASE + FLASH_ROW_INSTANCE2 * (CY_SFLASH_SIZEOF_USERROW))
#define ADDR_FLASH_ROW_INSTANCE(x)    (CY_SFLASH_USERBASE + (x)*(CY_SFLASH_SIZEOF_USERROW))

/*** Example of the used flash sections ****/
// HandlingStructure | User Data | System Data

//! Addresses inside of rows for storing of 
#define OFFSET_IN_ROW_HANDLING_STRUCT  (0)

//Offset is equal to the size of the handling structure
#define OFFSET_IN_ROW_USER_DATA        (sizeof(tsSettingsHandlingStruct))

//Offset is equal to the size of the half of the user row flash size
#define OFFSET_IN_ROW_SYSTEM_DATA      (CY_SFLASH_SIZEOF_USERROW / 2)

/****************************************** Variables ****************************************************/
//! Struct placed at start of memory row to hold identification information
typedef struct __attribute__((packed, aligned(1)))
{
    uint32_t ulMagicNumber;               /*< Magic number to recognize programmed flash memory row */
    uint32_t ulWriteIndex;                /*< Continuously running from 1 to recognize oldest and newest */
    uint8_t ucCRC_UserSettings;           /*< Checksum for user settings. CRC8 calculation */
    uint8_t ucCRC_SystemSettings;         /*< Checksum for system settings. CRC8 calculation */
} tsSettingsHandlingStruct;


//! Enum for accessing of some values in the module internal functions
typedef enum
{
    eSettingsHandlingStruct,
    eUserSettings,
    eSystemSettings,
    eWhole,
} teSettingsStorageEntry;



/****************************************** Function prototypes ******************************************/
static uint8_t Calculate_CRC8(const uint8_t* pucData, uint16_t uiLength);
static u8 GetNewestFlashRow(void);
static u8 GetNextWriteFlashRow(void);
static u32 GetAbsolutAddressInFlash(const teSettingsStorageEntry eEntry, const u8 ucRowIdx);
static teSFlashReturn GetNewestFromFlash(const teSettingsStorageEntry eEntry, void* pvData, u16 uiSize);
static void PrepareWriteBuffer(const teSettingsStorageEntry eEntry, u8* pucWriteBuffer, void* pvData, u8 ucSize, u8* pucWriteRow);
static teSFlashReturn CastSFlashReturnValue(const u32 ulCySysFlashReturn);
/****************************************** local functions *********************************************/
//********************************************************************************
/*!
\brief      Calculates an 8Bit CRC
\return     ucCRC - The 8 bit CRC for the given data
\param      pucData - Reference to the data which shall be used for CRC calculation
\param      uiLength - The amount of data which shall be calculated
***********************************************************************************/
static uint8_t Calculate_CRC8(const uint8_t* pucData, uint16_t uiLength)
{
    uint8_t ucCRC = 0x00;
    uint8_t ucExtract;
    uint8_t ucSum;
    
    for(int idx = 0; idx < uiLength; idx++)
    {
        ucExtract = *pucData;
        
        for(uint8_t tempI = 8; tempI; tempI--)
        {
            ucSum = (ucCRC ^ ucExtract) & 0x01;
            ucCRC >>= 1;
            
            if(ucSum)
            {
                ucCRC ^= 0x8C;
            }
            
            ucExtract >>= 1;
        }
        pucData++;
    }
    return ucCRC;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       31.03.2021
\brief      Determines the flash row index with the newest saved data.
            The settings handler structure is read from each user flash row. 
            Afterwards the magic number is checked. When no magic number is written, than
            this row is still empty or unused.
            The write index is compared with the "max-counter-index" to find the newest entry.
            Further checks are done with the magic number and the index.
\return     ucNewestRow - Either a value between 0 and (CY_SFLASH_NUMBER_USERROWS-1) or when
                          no entry was found 0xFF.
\param      none
***********************************************************************************/
static u8 GetNewestFlashRow(void)
{   
    u32 ulCmpWriteIdx = 0;
    u8 ucNewestRow = 0xFF;
    
    /* Go trough all rows and check them out */
    for(u8 ucRowInstance = 0; ucRowInstance < CY_SFLASH_NUMBER_USERROWS; ucRowInstance++)
    {
        /* Get the pointer to the settings handler structure */
        volatile tsSettingsHandlingStruct* psHandleStr = (volatile tsSettingsHandlingStruct*)(ADDR_FLASH_ROW_INSTANCE(ucRowInstance) + OFFSET_IN_ROW_HANDLING_STRUCT);
        
        /* Check if magic number is already written */
        if(psHandleStr->ulMagicNumber == WRITTEN_MAGIC_NUMBER)
        {
            /* Search for the highest write index */
            if(psHandleStr->ulWriteIndex > ulCmpWriteIdx)
            {
                /* Save highest write index and the dependent row instance */
                ulCmpWriteIdx = psHandleStr->ulWriteIndex;
                ucNewestRow = ucRowInstance;
            }
        }
    }    
    return ucNewestRow;
}

//********************************************************************************
/*!
\author     Kraemer E.
\date       31.03.2021
\brief      Determines the flash row index where the next data should be written to.
            The settings handler structure is read from each user flash row. 
            Afterwards the magic number is checked. When no magic number is written, than
            this row is still empty or unused.
            The write index is compared with the "min-counter-index" to find the oldest entry.
            Further checks are done with the magic number and the index.
\return     ucNextRow - A value between 0 and (CY_SFLASH_NUMBER_USERROWS-1)
\param      none
***********************************************************************************/
static u8 GetNextWriteFlashRow(void)
{   
    u32 ulCmpWriteIdx = UINT32_MAX;
    u8 ucNextRow = 0xFF;
    
    /* Go trough all rows and check them out */
    for(u8 ucRowInstance = 0; ucRowInstance < CY_SFLASH_NUMBER_USERROWS; ucRowInstance++)
    {
        /* Get the pointer to the settings handler structure */
        volatile tsSettingsHandlingStruct* psHandleStr = (volatile tsSettingsHandlingStruct*)(ADDR_FLASH_ROW_INSTANCE(ucRowInstance) + OFFSET_IN_ROW_HANDLING_STRUCT);
        
        /* Check if magic number is already written */
        if(psHandleStr->ulMagicNumber == WRITTEN_MAGIC_NUMBER)
        {
            /* Search for the lowest write index */
            if(psHandleStr->ulWriteIndex < ulCmpWriteIdx)
            {
                /* Save lowest write index and the dependent row instance */
                ulCmpWriteIdx = psHandleStr->ulWriteIndex;
                ucNextRow = ucRowInstance;
            }
        }
        else
        {
            ucNextRow = ucRowInstance;
            break;
        }
    }    
    return ucNextRow;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       31.03.2021
\brief      Returns the absolut address of the requested entry.
\return     ulAddr - Address of the entry instance
\param      eEntry - The requested entry
\param      ucRowIdx - The row index which should be used
***********************************************************************************/
static u32 GetAbsolutAddressInFlash(const teSettingsStorageEntry eEntry, const u8 ucRowIdx)
{
    /* Get the main address of the row instance */
    u32 ulAddr = ADDR_FLASH_ROW_INSTANCE(ucRowIdx);
    
    /* Add the offset of the entry to get the absolute address */
    switch(eEntry)
    {
        case eSettingsHandlingStruct:
            ulAddr += OFFSET_IN_ROW_HANDLING_STRUCT;
            break;
        
        case eUserSettings:
            ulAddr += OFFSET_IN_ROW_USER_DATA;
            break;
        
        case eSystemSettings:
            ulAddr += OFFSET_IN_ROW_SYSTEM_DATA;
            break;
        
        case eWhole:
            break;
    }
    
    return ulAddr;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       21.10.2019
\brief      Reads the requested entry from the newest flash row.
            Checks also the checksum.
\return     bCorrectRead - True when checksum was correct.
\param      eEntry - The requested entry which should be read
\param      pvData - The pointer to the buffer storage
\param      uiSize - The size of the buffer storage
***********************************************************************************/
static teSFlashReturn GetNewestFromFlash(const teSettingsStorageEntry eEntry, void* pvData, u16 uiSize)
{
    teSFlashReturn eReturn = eSFLASH_Invalid;
    
    /* Check for valid pointer */
    if(pvData)
    {
        const u8 ucNewestRow = GetNewestFlashRow();
        
        /* Check if row is valid */
        if(ucNewestRow < CY_SFLASH_NUMBER_USERROWS)
        {
            /* Get the address of the entry */
            u32 ulAddr = GetAbsolutAddressInFlash(eEntry, ucNewestRow);
            
            /* Copy stored data into local storage first */
            uint8_t aucLocalBuffer[CY_SFLASH_SIZEOF_USERROW];
            memcpy(&aucLocalBuffer[0], (void*)ulAddr, uiSize);
                    
            /* Calculate and check the checksum for USER- or SYSTEM-Settings. When the required entry is only the "handling struct" set a correct read */
            if(eEntry == eUserSettings || eEntry == eSystemSettings)
            {
                /* Read the settings handler structure from the row */
                tsSettingsHandlingStruct* psHandler = (tsSettingsHandlingStruct*)GetAbsolutAddressInFlash(eSettingsHandlingStruct, ucNewestRow);
                
                /* Calculate the checksum over the data */
                uint8_t ucCrc = Calculate_CRC8(&aucLocalBuffer[0], uiSize);
                
                /* Compare the checksum according to User- or System-Settings */
                if(eEntry == eUserSettings 
                    && psHandler->ucCRC_UserSettings == ucCrc)
                {
                    eReturn = eSFLASH_Success;
                }
                else if(eEntry == eSystemSettings
                    && psHandler->ucCRC_SystemSettings == ucCrc)
                {
                    eReturn = eSFLASH_Success;
                }            
                else
                {
                    eReturn = eSFLASH_CrcReadFault;
                }
               
                //When the CRC check was valid copy the data into the given reference
                if(eReturn == eSFLASH_Success)
                {
                    memcpy(pvData, &aucLocalBuffer[0], uiSize);
                }
            }
            else
            {
                eReturn = eSFLASH_Success;
            }
        }
        else
        {
            //No flash-row was found. 
            eReturn = eSFLASH_Unused;
        }
    }
    
    return eReturn;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       21.10.2019
\brief      Prepares the flash row for writting the new values into it. Reads the 
            newest flash row and saves the data into the buffer. Modifies the
            values in the SettingsHandlingStruct and also determines which row
            should be use for writting.
\return     none
\param      eEntry - The requested entry which should be written 
            (eUserSettings/eSystemSettings)
\param      pucWriteBuffer - The pointer to the write buffer in size of one flash row
\param      pvData - Pointer on userSettings or on system settings
\param      ucSize - Size of the data block in pvData
\param      pucWriteRow - Pointer to the variables which should be stored.
***********************************************************************************/
static void PrepareWriteBuffer(const teSettingsStorageEntry eEntry, u8* pucWriteBuffer, void* pvData, u8 ucSize, u8* pucWriteRow)
{
    /* Only User- or System-Settings are allowed to be written into the flash */
    if(eEntry == eUserSettings || eEntry == eSystemSettings)
    {
        /* Set pointer to the settings in the given write buffer */
        tsSettingsHandlingStruct* psSettings = (tsSettingsHandlingStruct*)&pucWriteBuffer[OFFSET_IN_ROW_HANDLING_STRUCT];
        
        /* Determine which flash row should be used for storage */
        *pucWriteRow = GetNextWriteFlashRow();
    
        /* Try to read entry from flash. When nothing is written yet, the functions returns false */
        if(GetNewestFromFlash(eWhole, pucWriteBuffer, CY_SFLASH_SIZEOF_USERROW))
        {
            /* Valid read. Therefore the index can be incremented */
            ++psSettings->ulWriteIndex;
        }
        else
        {
            /* Invalid read. Therefore the write index is started at 1 again */
            psSettings->ulWriteIndex = 1;
        }            
        psSettings->ulMagicNumber = WRITTEN_MAGIC_NUMBER;
        
        /* Calculate checksum */
        uint8_t ucCrc = Calculate_CRC8(pvData, ucSize);
        
        /* Update checksum in header and copy user data */
        if(eEntry == eUserSettings)
        {
            psSettings->ucCRC_UserSettings = ucCrc;
            memcpy(&pucWriteBuffer[OFFSET_IN_ROW_USER_DATA], pvData, ucSize);
        }
        else
        {    
            psSettings->ucCRC_SystemSettings = ucCrc;
            memcpy(&pucWriteBuffer[OFFSET_IN_ROW_SYSTEM_DATA], pvData, ucSize);
        }
    }  
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       01.04.2021
\brief      Simply casts the flash return value.
\return     eCast - Returns the casted enum value of the process.
\param      ulCySysFlashReturn - The return value from the cypress user flash process.
***********************************************************************************/
static teSFlashReturn CastSFlashReturnValue(const u32 ulCySysFlashReturn)
{
    teSFlashReturn eCast;
    
    switch(ulCySysFlashReturn)
    {
        case CY_SYS_SFLASH_SUCCESS:
            eCast = eSFLASH_Success;
        break;
        
        case CY_SYS_SFLASH_PROTECTED:
            eCast = eSFLASH_Protected;
        break;
        
        case CY_SYS_SFLASH_INVALID_ADDR:
            eCast = eSFLASH_InvalidAddress;
        break;
        
        default:
            eCast = eSFLASH_Invalid;
        break;
    }
    
    return eCast;
}

/****************************************** External visible functiones **********************************/
//********************************************************************************
/*!
\author     Kraemer E.
\date       20.01.2019
\brief      Erases all rows by overwriting the data with 0xFF for each byte. 
            Since there are not API function for erasing just writes 0xFF there
\return     none
\param      none
***********************************************************************************/
void HAL_Flash_EraseAll(void)
{
    u8 aucWriteBuffer[CY_SFLASH_SIZEOF_USERROW-1];
    memset(aucWriteBuffer, 0xFF, sizeof(aucWriteBuffer));

    for(u8 ucRowInstance = 0; ucRowInstance < CY_SFLASH_NUMBER_USERROWS; ucRowInstance++)
    {
        CySysSFlashWriteUserRow(ucRowInstance, aucWriteBuffer);
    }
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       20.01.2019
\brief      Gets user settings from nonvolatile memory. Also checks the checksum.
\return     teSFlashReturn - Enumeration of the flash access
\param      pvData - Pointer on user settings data block for reading
\param      ucSize - Size of user settings data block
***********************************************************************************/
teSFlashReturn HAL_Flash_GetUserSettings(void* pvData, u8 ucSize)
{
    return GetNewestFromFlash(eUserSettings, pvData, ucSize);
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       21.10.2019
\brief      Gets system settings from nonvolatile memory. Also checks the checksum.
\return     teSFlashReturn - Enumeration of the flash access
\param      pvData - Pointer on user settings data block for reading
\param      ucSize - Size of user settings data block
***********************************************************************************/
teSFlashReturn HAL_Flash_GetSystemSettings(void* pvData, u8 ucSize)
{
    return GetNewestFromFlash(eSystemSettings, pvData, ucSize);
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       01.04.2021
\brief      Writes the user settings into nonvolatile memory. Flash write is done
            within a criticial section.
\return     eRet - Enum value which describes the status of the process.
\param      pvData - Pointer on user settings data block to be written
\param      ucSize - Size of user settings data block
***********************************************************************************/
teSFlashReturn HAL_Flash_WriteUserSettings(void* pvData, u8 ucSize)
{    
    teSFlashReturn eRet = eSFLASH_Invalid;
    
    u8 ucWriteBuffer[CY_SFLASH_SIZEOF_USERROW];
    u8 ucWriteRow = 0xFF;
    
    /* Prevent out of bounds write access */
    if(ucSize > (OFFSET_IN_ROW_SYSTEM_DATA - OFFSET_IN_ROW_USER_DATA))
    {
        eRet = eSFLASH_DataToBigForSection;
    }
    else
    {    
        /* Clear write buffer */
        memset(&ucWriteBuffer, 0, sizeof(ucWriteBuffer));
        
        /* Prepare write buffer */
        PrepareWriteBuffer(eUserSettings, &ucWriteBuffer[0], pvData, ucSize, &ucWriteRow);
        
        /* Write buffer into flash */
        const u32 ulFlashWrRet = CySysSFlashWriteUserRow(ucWriteRow, ucWriteBuffer);
        eRet = CastSFlashReturnValue(ulFlashWrRet);
    }
    return eRet;
}

//********************************************************************************
/*!
\author     Kraemer E.
\date       01.04.2021
\brief      Writes the system settings into nonvolatile memory.
\return     eRet - Enum value which describes the status of the process.
\param      pvData - Pointer on user settings data block to be written
\param      ucSize - Size of user settings data block
***********************************************************************************/
teSFlashReturn HAL_Flash_WriteSystemSettings(void* pvData, u8 ucSize)
{    
    teSFlashReturn eRet = eSFLASH_Invalid;
    
    u8 ucWriteBuffer[CY_SFLASH_SIZEOF_USERROW];
    u8 ucWriteRow = 0xFF;
    
    /* Prevent out of bounds write access */
    if(ucSize > (CY_SFLASH_SIZEOF_USERROW - OFFSET_IN_ROW_SYSTEM_DATA))
    {
        eRet = eSFLASH_DataToBigForSection;
    }
    else
    {
        /* Clear write buffer */
        memset(&ucWriteBuffer, 0, sizeof(ucWriteBuffer));
        
        /* Prepare write buffer */
        PrepareWriteBuffer(eSystemSettings, &ucWriteBuffer[0], pvData, ucSize, &ucWriteRow);
        
        /* Write buffer into flash */
        const u32 ulFlashWrRet = CySysSFlashWriteUserRow(ucWriteRow, ucWriteBuffer);
        eRet = CastSFlashReturnValue(ulFlashWrRet);
    }
    return eRet;
}

#endif //PSOC_4100S_PLUS