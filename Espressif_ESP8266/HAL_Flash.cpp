//********************************************************************************
/*!
\author     
\date       

\file       HAL_Flash
\brief      Interface of module for storing of user and system settings into device flash.
            This part is called "User Supervisory Flash (SFLASH)".

\details    

//Used library: https://github.com/esp8266/Arduino/tree/master/libraries/EEPROM

// Normally writing to the 'emulated' EEPROM on ESP8266 requires an erase of the flash page used to hold
// the EEPROM data followed by a re-write of the changed data.
// The erasure takes a significant amount of time (10s of ms) and during this time
// interrupts must be blocked.
// In some cases this interferes with the sketch operation (e.g. it produces a noticeable
// blackout/flash for any PWM controlled lights as ESP8266 PWM relies on interrupts)
//
// The ESP_EEPROM library writes each new version of the EEPROM data to a new area until flash
// is full and so avoids wiping flash until necessary
//
// It's best for use when there are only a few things to save in EEPROM
// (i.e. total the size of the saved info is much smaller than the available flash size)

*/
            
#include "TargetConfig.h"
#ifdef ESPRESSIF_ESP8266
            
#include <string.h>
#include "HAL_Flash.h"
#include <ESP_EEPROM.h>

/****************************************** Defines ******************************************************/
#define WRITTEN_MAGIC_NUMBER (0xCAFEBABE) /*< Magic numbers for written row */

//The used size for the EEPROM
#define SFLASH_SIZEOF_USERROW     256u  

/*** Example of the used flash sections ****/
// HandlingStructure | User Data | System Data

//! Addresses inside of rows for storing of 
#define OFFSET_IN_ROW_HANDLING_STRUCT  (0)

//Offset is equal to the size of the handling structure
#define OFFSET_IN_ROW_USER_DATA        (sizeof(tsSettingsHandlingStruct))

//Offset is equal to the size of the half of the user row flash size
#define OFFSET_IN_ROW_SYSTEM_DATA      (SFLASH_SIZEOF_USERROW / 2)

 

/****************************************** Variables ****************************************************/
//! Struct placed at start of memory row to hold identification information
typedef struct __attribute__((packed, aligned(1)))
{
    u32 ulMagicNumber;               /*< Magic number to recognize programmed flash memory row */
    u32 ulWriteIndex;                /*< Continuously running from 1 to recognize oldest and newest */
    u16 uiSumUserSettings;           /*< Checksum for user settings. Just sum for values */
    u16 uiSumSystemSettings;         /*< Checksum for system settings. Just sum for values */
} tsSettingsHandlingStruct;


//! Enum for accessing of some values in the module internal functions
typedef enum
{
    eSettingsHandlingStruct,
    eUserSettings,
    eSystemSettings,
    eWhole,
} teSettingsStorageEntry;


static bool bFlashInitDone = false;

/****************************************** Function prototypes ******************************************/
static bool FlashBegin(u16 uiFlashSize);
static u32 GetAbsolutAddressInFlash(const teSettingsStorageEntry eEntry);
static teSFlashReturn GetNewestFromFlash(const teSettingsStorageEntry eEntry, void* pvData, u16 uiSize);
static void PrepareWriteBuffer(const teSettingsStorageEntry eEntry, u8* pucWriteBuffer, void* pvData, u8 ucSize);
static teSFlashReturn CastSFlashReturnValue(const bool bProcessStatus);

/****************************************** local functions *********************************************/
//********************************************************************************
/*!
\author     Kraemer E.
\date       24.06.2021
\brief      Initializes the EEPROM of the ESP.
\return     bFlashInitDone - True when Flash has been initialized
\param      uiFlashSize - The flash size which shall be reserved for EEPROM access.
***********************************************************************************/
static bool FlashBegin(u16 uiFlashSize)
{
    if(bFlashInitDone == false)
    {
        EEPROM.begin(uiFlashSize);
        bFlashInitDone = true;
    }

    return bFlashInitDone;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       31.03.2021
\brief      Returns the absolut address of the requested entry.
\return     ulAddr - Address of the entry instance
\param      eEntry - The requested entry
\param      ucRowIdx - The row index which should be used
***********************************************************************************/
static u32 GetAbsolutAddressInFlash(const teSettingsStorageEntry eEntry)
{
    /* Get the main address of the row instance */
    u32 ulAddr = 0;
    
    /* Add the offset of the entry to get the absolute address */
    switch(eEntry)
    {
        case eSettingsHandlingStruct:
            ulAddr += OFFSET_IN_ROW_HANDLING_STRUCT;
            break;
        
        case eUserSettings:
            ulAddr += OFFSET_IN_ROW_USER_DATA;
            break;
        
        case eSystemSettings:
            ulAddr += OFFSET_IN_ROW_SYSTEM_DATA;
            break;
        
        case eWhole:
            break;
    }
    
    return ulAddr;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       21.10.2019
\brief      Reads the requested entry from the newest flash row.
            Checks also the checksum.
\return     bCorrectRead - True when checksum was correct.
\param      eEntry - The requested entry which should be read
\param      pvData - The pointer to the buffer storage
\param      uiSize - The size of the buffer storage
***********************************************************************************/
static teSFlashReturn GetNewestFromFlash(const teSettingsStorageEntry eEntry, void* pvData, u16 uiSize)
{
    teSFlashReturn eReturn = eSFLASH_Invalid;
    
    /* Check for valid pointer */
    if(pvData)
    {
        u8 aucReadBuffer[SFLASH_SIZEOF_USERROW];
        memset(&aucReadBuffer[0], 0, sizeof(aucReadBuffer));

        /* Get saved values from EEPROM */
        EEPROM.get(0, aucReadBuffer);

        /* Get the data offset of the entry */
        u32 ulOffset = GetAbsolutAddressInFlash(eEntry);
        
        /* Copy stored data from flash into pointed buffer */
        memcpy(pvData, &aucReadBuffer[ulOffset], uiSize);
                
        /* Calculate and check the checksum for USER- or SYSTEM-Settings. When the required entry is only the "handling struct" set a correct read */
        if(eEntry == eUserSettings || eEntry == eSystemSettings)
        {
            /* Read the settings handler structure from the row */
            ulOffset = GetAbsolutAddressInFlash(eSettingsHandlingStruct);
            tsSettingsHandlingStruct* psHandler = (tsSettingsHandlingStruct*)&aucReadBuffer[ulOffset];
            
            /* Calculate the checksum over the data */
            u16 uiSum = 0;
            u16 uiIdx;            
            for(uiIdx = 0; uiIdx < uiSize; uiIdx++)
            {
                uiSum += *((u8*)(pvData) + uiIdx);
            }
            
            /* Compare the checksum according to User- or System-Settings */
            if(eEntry == eUserSettings 
                && psHandler->uiSumUserSettings == uiSum)
            {
                eReturn = eSFLASH_Success;
            }
            else if(eEntry == eSystemSettings
                && psHandler->uiSumSystemSettings == uiSum)
            {
                eReturn = eSFLASH_Success;
            }            
            else
            {
                eReturn = eSFLASH_CrcReadFault;
            }
        }
        else
        {
            eReturn = eSFLASH_Success;
        } 
    }
    
    return eReturn;
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       21.10.2019
\brief      Prepares the flash row for writting the new values into it. Reads the 
            newest flash row and saves the data into the buffer. Modifies the
            values in the SettingsHandlingStruct and also determines which row
            should be use for writting.
\return     none
\param      eEntry - The requested entry which should be written 
            (eUserSettings/eSystemSettings)
\param      pucWriteBuffer - The pointer to the write buffer in size of one flash row
\param      pvData - Pointer on userSettings or on system settings
\param      ucSize - Size of the data block in pvData
\param      pucWriteRow - Pointer to the variables which should be stored.
***********************************************************************************/
static void PrepareWriteBuffer(const teSettingsStorageEntry eEntry, u8* pucWriteBuffer, void* pvData, u8 ucSize)
{
    /* Only User- or System-Settings are allowed to be written into the flash */
    if(eEntry == eUserSettings || eEntry == eSystemSettings)
    {
        /* Set pointer to the settings in the given write buffer */
        tsSettingsHandlingStruct* psSettings = (tsSettingsHandlingStruct*)&pucWriteBuffer[OFFSET_IN_ROW_HANDLING_STRUCT];
            
        /* Try to read entry from flash. When nothing is written yet, the functions returns false */
        if(GetNewestFromFlash(eWhole, pucWriteBuffer, SFLASH_SIZEOF_USERROW))
        {
            /* Valid read. Therefore the index can be incremented */
            ++psSettings->ulWriteIndex;
        }
        else
        {
            /* Invalid read. Therefore the write index is started at 1 again */
            psSettings->ulWriteIndex = 1;
        }            
        psSettings->ulMagicNumber = WRITTEN_MAGIC_NUMBER;
        
        
        /* Calculate checksum */
        u8 ucIdx;
        u16 uiSum = 0;
        for(ucIdx = 0; ucIdx < ucSize; ucIdx++)
        {
            uiSum += *((u8*)(pvData) + ucIdx);
        }

        /* Update checksum in header and copy user data */
        if(eEntry == eUserSettings)
        {
            psSettings->uiSumUserSettings = uiSum;
            memcpy(&pucWriteBuffer[OFFSET_IN_ROW_USER_DATA], pvData, ucSize);
        }
        else
        {    
            psSettings->uiSumSystemSettings = uiSum;
            memcpy(&pucWriteBuffer[OFFSET_IN_ROW_SYSTEM_DATA], pvData, ucSize);
        }
    }  
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       01.04.2021
\brief      Simply casts the flash return value.
\return     eCast - Returns the casted enum value of the process.
\param      bProcessStatus - The return value from the user flash process.
***********************************************************************************/
static teSFlashReturn CastSFlashReturnValue(const bool bProcessStatus)
{
    teSFlashReturn eCast = (bProcessStatus == true) ? eSFLASH_Success : eSFLASH_Protected;
    return eCast;
}

/****************************************** External visible functiones **********************************/
//********************************************************************************
/*!
\author     Kraemer E.
\date       24.06.2021
\brief      Erases all reserved eeprom.
\return     none
\param      none
***********************************************************************************/
void HAL_Flash_EraseAll(void)
{
    //Flash begin has to be called before actual process starts
    FlashBegin(SFLASH_SIZEOF_USERROW);

    //Delete everything from EEPROM
    EEPROM.wipe();    
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       20.01.2019
\brief      Gets user settings from nonvolatile memory. Also checks the checksum.
\return     teSFlashReturn - Enumeration of the flash access
\param      pvData - Pointer on user settings data block for reading
\param      ucSize - Size of user settings data block
***********************************************************************************/
teSFlashReturn HAL_Flash_GetUserSettings(void* pvData, u8 ucSize)
{
    return GetNewestFromFlash(eUserSettings, pvData, ucSize);
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       21.10.2019
\brief      Gets system settings from nonvolatile memory. Also checks the checksum.
\return     teSFlashReturn - Enumeration of the flash access
\param      pvData - Pointer on user settings data block for reading
\param      ucSize - Size of user settings data block
***********************************************************************************/
teSFlashReturn HAL_Flash_GetSystemSettings(void* pvData, u8 ucSize)
{
    return GetNewestFromFlash(eSystemSettings, pvData, ucSize);
}


//********************************************************************************
/*!
\author     Kraemer E.
\date       01.04.2021
\brief      Writes the user settings into nonvolatile memory. Flash write is done
            within a criticial section.
\return     eRet - Enum value which describes the status of the process.
\param      pvData - Pointer on user settings data block to be written
\param      ucSize - Size of user settings data block
***********************************************************************************/
teSFlashReturn HAL_Flash_WriteUserSettings(void* pvData, u8 ucSize)
{    
    teSFlashReturn eRet = eSFLASH_Invalid;    
    u8 aucWriteBuffer[SFLASH_SIZEOF_USERROW];
    
    /* Prevent out of bounds write access */
    if(ucSize > (OFFSET_IN_ROW_SYSTEM_DATA - OFFSET_IN_ROW_USER_DATA))
    {
        eRet = eSFLASH_DataToBigForSection;
    }
    else
    {    
        /* Clear write buffer */
        memset(&aucWriteBuffer, 0, sizeof(aucWriteBuffer));
        
        /* Prepare write buffer */
        PrepareWriteBuffer(eUserSettings, &aucWriteBuffer[0], pvData, ucSize);
        
        //CAll flash begin every time. When it's already initialized
        //no problem will occur
        FlashBegin(SFLASH_SIZEOF_USERROW);

        //Put data first into the EEPROM BUFFER
        EEPROM.put(0, aucWriteBuffer);

        /* Write buffer into flash */
        bool bOk = EEPROM.commit();

        eRet = CastSFlashReturnValue(bOk);
    }
    return eRet;
}

//********************************************************************************
/*!
\author     Kraemer E.
\date       01.04.2021
\brief      Writes the system settings into nonvolatile memory.
\return     eRet - Enum value which describes the status of the process.
\param      pvData - Pointer on user settings data block to be written
\param      ucSize - Size of user settings data block
***********************************************************************************/
teSFlashReturn HAL_Flash_WriteSystemSettings(void* pvData, u8 ucSize)
{    
    teSFlashReturn eRet = eSFLASH_Invalid;
    
    u8 aucWriteBuffer[SFLASH_SIZEOF_USERROW];
    
    /* Prevent out of bounds write access */
    if(ucSize > (SFLASH_SIZEOF_USERROW - OFFSET_IN_ROW_SYSTEM_DATA))
    {
        eRet = eSFLASH_DataToBigForSection;
    }
    else
    {
        /* Clear write buffer */
        memset(&aucWriteBuffer, 0, sizeof(aucWriteBuffer));
        
        /* Prepare write buffer */
        PrepareWriteBuffer(eSystemSettings, &aucWriteBuffer[0], pvData, ucSize);
        
        //CAll flash begin every time. When it's already initialized
        //no problem will occur
        FlashBegin(SFLASH_SIZEOF_USERROW);

        //Put data first into the EEPROM BUFFER
        EEPROM.put(0, aucWriteBuffer);

        /* Write buffer into flash */
        bool bOk = EEPROM.commit();

        eRet = CastSFlashReturnValue(bOk);
    }
    return eRet;
}

#endif //PSOC_4100S_PLUS