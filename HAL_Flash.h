/* ========================================
 *
 * Copyright Eduard Kraemer, 2019
 *
 * ========================================
*/
#ifndef _HAL_FLASH_H_
#define _HAL_FLASH_H_

#ifdef __cplusplus
extern "C"
{
#endif   


#include "BaseTypes.h"

typedef enum
{
    eSFLASH_Success,            /* When flash write and read process was successful */
    eSFLASH_InvalidAddress,     /* When the used flash address is invalid */
    eSFLASH_Protected,          /* When the flash address has a write protection */
    eSFLASH_DataToBigForSection,/* When the used Data is greater than the allowed limit */
    eSFLASH_DataToBigForRow,    /* When the used data is greater than the flash row */
    eSFLASH_BufferToSmall,      /* When the given buffer to read from the flash is to small */
    eSFLASH_CrcReadFault,       /* The calculated CRC from data differs from the read CRC */
    eSFLASH_Unused,             /* No used flash row has been found */
    eSFLASH_Invalid             /* Fault couldn't be derived */
}teSFlashReturn;


void HAL_Flash_EraseAll(void);
teSFlashReturn HAL_Flash_GetUserSettings(void* pvData, u8 ucSize);
teSFlashReturn HAL_Flash_GetSystemSettings(void* pvData, u8 ucSize);
teSFlashReturn HAL_Flash_WriteUserSettings(void* pvData, u8 ucSize);
teSFlashReturn HAL_Flash_WriteSystemSettings(void* pvData, u8 ucSize);

#ifdef __cplusplus
}
#endif    

#endif //_HAL_FLASH_H_